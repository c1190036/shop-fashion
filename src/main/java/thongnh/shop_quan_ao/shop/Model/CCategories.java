package thongnh.shop_quan_ao.shop.Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "categories")
public class CCategories {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "categories")
    private String categories;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "product_categories", joinColumns = {
            @JoinColumn(name = "categories_id") }, inverseJoinColumns = {
                    @JoinColumn(name = "product_id") })
    @JsonIgnore
    private Set<CProduct> products = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "blog_categories", joinColumns = {
            @JoinColumn(name = "categories_id") }, inverseJoinColumns = {
                    @JoinColumn(name = "blog_id") })
    @JsonIgnore
    private Set<CBlog> blog = new HashSet<>();

    public CCategories() {
    }

    public CCategories(Long id, String categories, Set<CProduct> products, Set<CBlog> blog) {
        this.id = id;
        this.categories = categories;
        this.products = products;
        this.blog = blog;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public Set<CProduct> getProducts() {
        return products;
    }

    public void setProducts(Set<CProduct> products) {
        this.products = products;
    }

    public Set<CBlog> getBlog() {
        return blog;
    }

    public void setBlog(Set<CBlog> blog) {
        this.blog = blog;
    }

}
