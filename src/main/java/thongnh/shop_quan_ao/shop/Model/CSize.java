package thongnh.shop_quan_ao.shop.Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "size")
public class CSize {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "size")
    private String size;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "product_sizes", joinColumns = { @JoinColumn(name = "size_id") }, inverseJoinColumns = {
            @JoinColumn(name = "product_id") })
    @JsonIgnore
    private Set<CProduct> products = new HashSet<>();

    public CSize() {
    }

    public CSize(Long id, String size, Set<CProduct> products) {
        this.id = id;
        this.size = size;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Set<CProduct> getProducts() {
        return products;
    }

    public void setProducts(Set<CProduct> products) {
        this.products = products;
    }
    
}
