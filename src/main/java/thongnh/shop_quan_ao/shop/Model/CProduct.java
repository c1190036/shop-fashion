package thongnh.shop_quan_ao.shop.Model;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class CProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Long price;

    @Column(name = "discount")
    private Long discount;

    @Column(name = "stock")
    private Long stock;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_create")
    private Date dateCreate;

    @Column(name = "featured")
    private boolean featured = false;

    @Column(name = "url_img")
    private String url_img;

    @Column(name = "description", length = 2000)
    private String description;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "products")
    private Set<CStyles> styles = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "products")
    private Set<CSize> sizes = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "products")
    private Set<CCompositions> compositions = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "products")
    private Set<CBrands> brands = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "products")
    private Set<CBrands> categories = new HashSet<>();

    public CProduct() {
    }

    public CProduct(Long id, String name, Long price, Long discount, Long stock, Date dateCreate, boolean featured,
            String url_img, String description, Set<CStyles> styles, Set<CSize> sizes, Set<CCompositions> compositions,
            Set<CBrands> brands, Set<CBrands> categories) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.discount = discount;
        this.stock = stock;
        this.dateCreate = dateCreate;
        this.featured = featured;
        this.url_img = url_img;
        this.description = description;
        this.styles = styles;
        this.sizes = sizes;
        this.compositions = compositions;
        this.brands = brands;
        this.categories = categories;
    }

    public Set<CCompositions> getCompositions() {
        return compositions;
    }

    public void setCompositions(Set<CCompositions> compositions) {
        this.compositions = compositions;
    }

    public Set<CBrands> getBrands() {
        return brands;
    }

    public void setBrands(Set<CBrands> brands) {
        this.brands = brands;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public String getUrl_img() {
        return url_img;
    }

    public void setUrl_img(String url_img) {
        this.url_img = url_img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<CStyles> getStyles() {
        return styles;
    }

    public void setStyles(Set<CStyles> styles) {
        this.styles = styles;
    }

    public Set<CSize> getSizes() {
        return sizes;
    }

    public void setSizes(Set<CSize> sizes) {
        this.sizes = sizes;
    }

    public Set<CBrands> getCategories() {
        return categories;
    }

    public void setCategories(Set<CBrands> categories) {
        this.categories = categories;
    }

}
