package thongnh.shop_quan_ao.shop.Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "brands")
public class CBrands {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column(name = "brands")
        private String brands;

        @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
        @JoinTable(name = "product_brands", joinColumns = { @JoinColumn(name = "brands_id") }, inverseJoinColumns = {
                        @JoinColumn(name = "product_id") })
        @JsonIgnore
        private Set<CProduct> products = new HashSet<>();

        public CBrands() {
        }

        public CBrands(Long id, String brands, Set<CProduct> products) {
                this.id = id;
                this.brands = brands;
                this.products = products;
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public String getBrands() {
                return brands;
        }

        public void setBrands(String brands) {
                this.brands = brands;
        }

        public Set<CProduct> getProducts() {
                return products;
        }

        public void setProducts(Set<CProduct> products) {
                this.products = products;
        }

        

}
