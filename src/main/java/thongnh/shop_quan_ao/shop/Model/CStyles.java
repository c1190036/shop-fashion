package thongnh.shop_quan_ao.shop.Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "styles")
public class CStyles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "styles")
    private String styles;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "product_styles", joinColumns = { @JoinColumn(name = "styles_id") }, inverseJoinColumns = {
            @JoinColumn(name = "product_id") })
    @JsonIgnore
    private Set<CProduct> products = new HashSet<>();

    public CStyles() {
    }

    public CStyles(Long id, String styles, Set<CProduct> products) {
        this.id = id;
        this.styles = styles;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStyles() {
        return styles;
    }

    public void setStyles(String styles) {
        this.styles = styles;
    }

    public Set<CProduct> getProducts() {
        return products;
    }

    public void setProduct(Set<CProduct> products) {
        this.products = products;
    }

}
