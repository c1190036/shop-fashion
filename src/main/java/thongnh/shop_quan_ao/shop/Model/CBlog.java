package thongnh.shop_quan_ao.shop.Model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "blog")
public class CBlog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description", length = 2000)
    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_create")
    private Date dateCreate;

    @Column(name = "url_img")
    private String url_img;

    @Column(name = "content")
    private String content;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "blog")
    private Set<CCategories> categories = new HashSet<>();

    public CBlog() {
    }

    public CBlog(Long id, String name, String description, Date dateCreate, String url_img, String content,
            Set<CCategories> categories) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dateCreate = dateCreate;
        this.url_img = url_img;
        this.content = content;
        this.categories = categories;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getUrl_img() {
        return url_img;
    }

    public void setUrl_img(String url_img) {
        this.url_img = url_img;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Set<CCategories> getCategories() {
        return categories;
    }

    public void setCategories(Set<CCategories> categories) {
        this.categories = categories;
    }

}
