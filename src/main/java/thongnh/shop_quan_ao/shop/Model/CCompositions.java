package thongnh.shop_quan_ao.shop.Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "compositions")
public class CCompositions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "compositions")
    private String compositions;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "product_compositions", joinColumns = {
            @JoinColumn(name = "compositions_id") }, inverseJoinColumns = {
                    @JoinColumn(name = "product_id") })
    @JsonIgnore
    private Set<CProduct> products = new HashSet<>();

    public CCompositions() {
    }

    public CCompositions(Long id, String compositions, Set<CProduct> products) {
        this.id = id;
        this.compositions = compositions;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompositions() {
        return compositions;
    }

    public void setCompositions(String compositions) {
        this.compositions = compositions;
    }

    public Set<CProduct> getProducts() {
        return products;
    }

    public void setProducts(Set<CProduct> products) {
        this.products = products;
    }

}
