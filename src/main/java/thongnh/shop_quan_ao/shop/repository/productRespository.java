package thongnh.shop_quan_ao.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import thongnh.shop_quan_ao.shop.Model.CProduct;


public interface productRespository extends JpaRepository<CProduct,Long> {
} 
