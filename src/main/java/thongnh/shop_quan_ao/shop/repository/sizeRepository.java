package thongnh.shop_quan_ao.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import thongnh.shop_quan_ao.shop.Model.CSize;


public interface sizeRepository extends JpaRepository<CSize,Long> {
     
} 
