package thongnh.shop_quan_ao.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import thongnh.shop_quan_ao.shop.Model.CBlog;

public interface blogRepository extends JpaRepository<CBlog, Long> {

}
