package thongnh.shop_quan_ao.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import thongnh.shop_quan_ao.shop.Model.CBrands;

public interface brandsRepository extends JpaRepository<CBrands, Long> {

}
