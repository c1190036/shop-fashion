package thongnh.shop_quan_ao.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import thongnh.shop_quan_ao.shop.Model.CCategories;

public interface categoriesRepository extends JpaRepository<CCategories, Long> {

}
