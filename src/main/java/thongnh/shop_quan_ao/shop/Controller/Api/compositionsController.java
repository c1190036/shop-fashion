package thongnh.shop_quan_ao.shop.Controller.Api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import thongnh.shop_quan_ao.shop.Model.CCompositions;
import thongnh.shop_quan_ao.shop.service.Impl.compositionsServiceImpl;

@RestController
@CrossOrigin
public class compositionsController {
    @Autowired
    compositionsServiceImpl compositionsServiceImpl;

    @GetMapping("/compositions")
    public ResponseEntity<List<CCompositions>> getAllProduct() {
        try {
            List<CCompositions> compositions = compositionsServiceImpl.getAll();
            return new ResponseEntity<>(compositions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
