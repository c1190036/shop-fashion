package thongnh.shop_quan_ao.shop.Controller.Api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import thongnh.shop_quan_ao.shop.Model.CStyles;
import thongnh.shop_quan_ao.shop.service.Impl.stylesServiceImpl;

@RestController
@CrossOrigin
public class stylesController {
    @Autowired
    stylesServiceImpl stylesServiceImpl;

    @GetMapping("/styles")
    public ResponseEntity<List<CStyles>> getAllProduct() {
        try {
            List<CStyles> styles = stylesServiceImpl.getAll();
            return new ResponseEntity<>(styles, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
