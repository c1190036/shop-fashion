package thongnh.shop_quan_ao.shop.Controller.Api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import thongnh.shop_quan_ao.shop.Model.CBrands;
import thongnh.shop_quan_ao.shop.service.Impl.brandsServiceImpl;

@RestController
@CrossOrigin
public class brandsController {
    @Autowired
    brandsServiceImpl brandsServiceImpl;

    @GetMapping("/brands")
    public ResponseEntity<List<CBrands>> getAllProduct() {
        try {
            List<CBrands> brands = brandsServiceImpl.getAll();
            return new ResponseEntity<>(brands, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
