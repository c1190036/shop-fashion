package thongnh.shop_quan_ao.shop.Controller.Api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import thongnh.shop_quan_ao.shop.Model.CSize;
import thongnh.shop_quan_ao.shop.service.Impl.sizeServiceImpl;

@RestController
@CrossOrigin
public class sizeController {
    @Autowired
    sizeServiceImpl sizeServiceImpl;

    @GetMapping("/sizes")
    public ResponseEntity<List<CSize>> getAllProduct() {
        try {
            List<CSize> size = sizeServiceImpl.getAll();
            return new ResponseEntity<>(size, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
