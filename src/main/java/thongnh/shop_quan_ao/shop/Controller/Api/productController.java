package thongnh.shop_quan_ao.shop.Controller.Api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import thongnh.shop_quan_ao.shop.Model.CProduct;
import thongnh.shop_quan_ao.shop.service.Impl.productServiceImpl;

@RestController
@CrossOrigin
public class productController {
    @Autowired
    productServiceImpl productServiceImpl;

    @GetMapping("/product")
    public ResponseEntity<List<CProduct>> getAllProduct() {
        try {
            List<CProduct> products = productServiceImpl.getAll();
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
