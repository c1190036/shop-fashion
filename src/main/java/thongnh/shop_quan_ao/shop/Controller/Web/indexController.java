package thongnh.shop_quan_ao.shop.Controller.Web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import thongnh.shop_quan_ao.shop.Model.CProduct;
import thongnh.shop_quan_ao.shop.service.Impl.productServiceImpl;

@RestController
public class indexController {
    @Autowired
    productServiceImpl productServiceImpl;

    @RequestMapping(value = { "/", "/index" })
    public ModelAndView index(Model model) {
        ModelAndView modelAndView = new ModelAndView("index");
        List<CProduct> productsNew = productServiceImpl.getNewestProducts();
        List<CProduct> productFeatured = productServiceImpl.getFeatured();
        model.addAttribute("productsNew", productsNew);
        model.addAttribute("productFeatured", productFeatured);
        return modelAndView;
    }
}
