package thongnh.shop_quan_ao.shop.service;

import java.util.List;

import thongnh.shop_quan_ao.shop.Model.CProduct;

public interface productService {
    List<CProduct> getAll();
    CProduct getCProductById(Long id);
    void saveCProduct(CProduct cProduct);
    void deleteCProduct(Long id);
    List<CProduct> getFeatured();
    List<CProduct> getNewestProducts();
}
