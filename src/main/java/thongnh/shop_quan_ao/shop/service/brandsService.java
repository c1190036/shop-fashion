package thongnh.shop_quan_ao.shop.service;

import java.util.List;

import thongnh.shop_quan_ao.shop.Model.CBrands;

public interface brandsService {
    List<CBrands> getAll();
    CBrands getCBrandsById(Long id);
    void saveCBrands(CBrands cBrands);
    void deleteCBrands(Long id);
}
