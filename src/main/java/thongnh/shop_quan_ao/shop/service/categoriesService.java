package thongnh.shop_quan_ao.shop.service;

import java.util.List;

import thongnh.shop_quan_ao.shop.Model.CCategories;

public interface categoriesService {

    List<CCategories> getAll();

    CCategories getCCategoriesById(Long id);

    void saveCCategories(CCategories CCategories);

    void deleteCCategories(Long id);
}
