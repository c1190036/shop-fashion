package thongnh.shop_quan_ao.shop.service;

import java.util.List;

import thongnh.shop_quan_ao.shop.Model.CCompositions;

public interface compositionsService {
    List<CCompositions> getAll();
    CCompositions getCCompositionsById(Long id);
    void saveCCompositions(CCompositions cCompositions);
    void deleteCCompositions(Long id);
}
