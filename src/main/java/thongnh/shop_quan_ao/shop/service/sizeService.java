package thongnh.shop_quan_ao.shop.service;

import java.util.List;

import thongnh.shop_quan_ao.shop.Model.CSize;

public interface sizeService {
    List<CSize> getAll();
    CSize getCSizeById(Long id);
    void saveCSize(CSize cSize);
    void deleteCSize(Long id);
}
