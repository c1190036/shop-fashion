package thongnh.shop_quan_ao.shop.service;

import java.util.List;

import thongnh.shop_quan_ao.shop.Model.CStyles;

public interface stylesService {
    List<CStyles> getAll();
    CStyles getCStylesById(Long id);
    void saveCStyles(CStyles cStyles);
    void deleteCStyles(Long id);
}
