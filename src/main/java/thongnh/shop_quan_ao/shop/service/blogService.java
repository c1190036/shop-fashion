package thongnh.shop_quan_ao.shop.service;

import java.util.List;

import thongnh.shop_quan_ao.shop.Model.CBlog;

public interface blogService {

    List<CBlog> getAll();

    CBlog getCBlogById(Long id);

    void saveCBlog(CBlog CBlog);

    void deleteCBlog(Long id);
}
