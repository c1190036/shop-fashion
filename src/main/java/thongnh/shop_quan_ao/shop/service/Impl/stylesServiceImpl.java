package thongnh.shop_quan_ao.shop.service.Impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import thongnh.shop_quan_ao.shop.Model.CStyles;
import thongnh.shop_quan_ao.shop.repository.stylesRepository;
import thongnh.shop_quan_ao.shop.service.stylesService;

@Service
public class stylesServiceImpl implements stylesService {
  @Autowired
  stylesRepository stylesRepository;

  @Override
  public List<CStyles> getAll() {
    return stylesRepository.findAll();
  }

  @Override
  public CStyles getCStylesById(Long id) {
    Optional<CStyles> CStyles = stylesRepository.findById(id);
    if (CStyles.isPresent()) {
      return CStyles.get();
    } else {
      return null;
    }
  }

  @Override
  public void saveCStyles(CStyles cStyles) {
    stylesRepository.save(cStyles);
  }

  @Override
  public void deleteCStyles(Long id) {
    stylesRepository.deleteById(id);
  }

}
