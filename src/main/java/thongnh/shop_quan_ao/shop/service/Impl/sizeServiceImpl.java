package thongnh.shop_quan_ao.shop.service.Impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import thongnh.shop_quan_ao.shop.Model.CSize;

import thongnh.shop_quan_ao.shop.repository.sizeRepository;
import thongnh.shop_quan_ao.shop.service.sizeService;

@Service
public class sizeServiceImpl implements sizeService {
  @Autowired
  sizeRepository sizeRepository;

  @Override
  public List<CSize> getAll() {
    return sizeRepository.findAll();
  }

  @Override
  public CSize getCSizeById(Long id) {
    Optional<CSize> CSize = sizeRepository.findById(id);
    if (CSize.isPresent()) {
      return CSize.get();
    } else {
      return null;
    }
  }

  @Override
  public void saveCSize(CSize cSize) {
    sizeRepository.save(cSize);
  }

  @Override
  public void deleteCSize(Long id) {
    sizeRepository.deleteById(id);
  }

}
