package thongnh.shop_quan_ao.shop.service.Impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import thongnh.shop_quan_ao.shop.Model.CCompositions;
import thongnh.shop_quan_ao.shop.repository.compositionsRepository;
import thongnh.shop_quan_ao.shop.service.compositionsService;

@Service
public class compositionsServiceImpl implements compositionsService {
  @Autowired
  compositionsRepository compositionsRepository;

  @Override
  public List<CCompositions> getAll() {
    return compositionsRepository.findAll();
  }

  @Override
  public CCompositions getCCompositionsById(Long id) {
    Optional<CCompositions> CCompositions = compositionsRepository.findById(id);
    if (CCompositions.isPresent()) {
      return CCompositions.get();
    } else {
      return null;
    }
  }

  @Override
  public void saveCCompositions(CCompositions cCompositions) {
    compositionsRepository.save(cCompositions);
  }

  @Override
  public void deleteCCompositions(Long id) {
    compositionsRepository.deleteById(id);
  }

}
