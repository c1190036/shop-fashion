package thongnh.shop_quan_ao.shop.service.Impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import thongnh.shop_quan_ao.shop.Model.CProduct;
import thongnh.shop_quan_ao.shop.repository.productRespository;
import thongnh.shop_quan_ao.shop.service.productService;

@Service
public class productServiceImpl implements productService {
  @Autowired
  productRespository productRepository;

  @Override
  public List<CProduct> getAll() {
    return productRepository.findAll();
  }

  @Override
  public CProduct getCProductById(Long id) {
    Optional<CProduct> CProduct = productRepository.findById(id);
    if (CProduct.isPresent()) {
      return CProduct.get();
    } else {
      return null;
    }
  }

  @Override
  public void saveCProduct(CProduct cProduct) {
    productRepository.save(cProduct);
  }

  @Override
  public void deleteCProduct(Long id) {
    productRepository.deleteById(id);
  }

  @Override
  public List<CProduct> getFeatured() {
    List<CProduct> featuredProducts = productRepository.findAll()
        .stream()
        .filter(product -> product.isFeatured())
        .collect(Collectors.toList());
    return featuredProducts;
  }

  @Override
  public List<CProduct> getNewestProducts() {
    List<CProduct> newestProducts = productRepository.findAll()
        .stream()
        .sorted((p1, p2) -> p2.getDateCreate().compareTo(p1.getDateCreate()))
        .limit(10)
        .collect(Collectors.toList());
    return newestProducts;
  }

}
