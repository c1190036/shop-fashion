package thongnh.shop_quan_ao.shop.service.Impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import thongnh.shop_quan_ao.shop.Model.CBrands;

import thongnh.shop_quan_ao.shop.repository.brandsRepository;
import thongnh.shop_quan_ao.shop.service.brandsService;


@Service
public class brandsServiceImpl implements brandsService {
  @Autowired
  brandsRepository BrandsRepository;

  @Override
  public List<CBrands> getAll() {
    return BrandsRepository.findAll();
  }

  @Override
  public CBrands getCBrandsById(Long id) {
    Optional<CBrands> CBrands = BrandsRepository.findById(id);
    if (CBrands.isPresent()) {
      return CBrands.get();
    } else {
      return null;
    }
  }

  @Override
  public void saveCBrands(CBrands cBrands) {
    BrandsRepository.save(cBrands);
  }

  @Override
  public void deleteCBrands(Long id) {
    BrandsRepository.deleteById(id);
  }

}
