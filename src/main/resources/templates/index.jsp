﻿<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Coron - Fashion eCommerce Bootstrap4 Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.png">

    <!-- all css here -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/plugin.css">
    <link rel="stylesheet" href="/css/bundle.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/responsive.css">
    <script src="/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!-- Add your site or application content here -->

    <!--pos page start-->
    <div class="pos_page">
        <div class="container">
            <!--pos page inner-->
            <div class="pos_page_inner">
                <!--header area -->
                <div th:replace="~{fragments/header}"></div>
                <!--header end -->

                <!--pos home section-->
                <div class="pos_home_section">
                    <div class="row">
                        <!--banner slider start-->
                        <div class="col-12">
                            <div class="banner_slider slider_two">
                                <div class="slider_active owl-carousel">
                                    <div class="single_slider" style="background-image: url(/img/slider/slider_2.png)">
                                        <div class="slider_content">
                                            <div class="slider_content_inner">
                                                <h1>fashion for you</h1>
                                                <p>Lorem ipsum dolor sit amet, c onsectetur adipisicing elit. <br>
                                                    Cumque eligendi quia, ratione porro, nemo non.</p>
                                                <a href="#">shop now</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single_slider" style="background-image: url(/img/slider/slide_4.png)">
                                        <div class="slider_content">
                                            <div class="slider_content_inner">
                                                <h1>fashion for you</h1>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> Cumque
                                                    eligendi quia, ratione porro, nemo non.</p>
                                                <a href="#">shop now</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single_slider" style="background-image: url(/img/slider/slider_3.png)">
                                        <div class="slider_content">
                                            <div class="slider_content_inner">
                                                <h1>fashion for you</h1>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> Cumque
                                                    eligendi quia, ratione porro, nemo non.</p>
                                                <a href="#">shop now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--banner slider start-->
                        </div>
                    </div>
                    <!--new product area start-->
                    <div class="new_product_area product_two">
                        <div class="row">
                            <div class="col-12">
                                <div class="block_title">
                                    <h3>Sản phẩm mới</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="single_p_active owl-carousel">
                                <div class="col-lg-3" th:each="product:${productsNew}">
                                    <div class="single_product">
                                        <div class="product_thumb">
                                            <a th:href="@{'/product/'+${product.id}}">
                                                <img th:if="${not #strings.isEmpty(product.url_img)}"
                                                    th:src="${product.url_img}">
                                                <img th:if="${#strings.isEmpty(product.url_img)}"
                                                    src="/img/product/0/istockphoto-1409329028-170667a.jpg">
                                            </a>
                                            <div th:if="${product.featured==true}" class="hot_img">
                                                <img src="/img/cart/span-hot.png">
                                            </div>
                                            <div class="img_icone">
                                                <img src="/img/cart/span-new.png">
                                            </div>
                                            <div class="product_action">
                                                <a href="#"> <i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
                                            </div>
                                        </div>
                                        <div class="product_content">
                                            <span class="product_price" th:text="${product.price+' VNĐ'}"></span>
                                            <h3 class="product_title"><a th:href="@{'/product/'+${product.id}}"
                                                    th:text="${product.name}"></a></h3>
                                        </div>
                                        <div class="product_info">
                                            <ul>
                                                <li><a href="#" title="Thêm vào danh sách yêu thích">Yêu thích</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#modal_box"
                                                        title="Quick view">Xem Chi Tiết</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--new product area start-->

                    <!--banner area start-->
                    <div class="banner_area banner_two">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <div class="single_banner">
                                    <a href="#"><img src="/img/banner/banner7.jpg" alt=""></a>
                                    <div class="banner_title">
                                        <p>Up to <span> 40%</span> off</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single_banner">
                                    <a href="#"><img src="/img/banner/banner8.jpg" alt=""></a>
                                    <div class="banner_title title_2">
                                        <p>sale off <span> 30%</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="single_banner">
                                    <a href="#"><img src="/img/banner/banner11.jpg" alt=""></a>
                                    <div class="banner_title title_3">
                                        <p>sale off <span> 30%</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--banner area end-->

                    <!--featured product area start-->
                    <div class="new_product_area product_two">
                        <div class="row">
                            <div class="col-12">
                                <div class="block_title">
                                    <h3>Sản phẩm tiêu biểu (<span th:text="${productFeatured.size()}"></span>)</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="single_p_active owl-carousel">
                                <div class="col-lg-3" th:each="product:${productFeatured}">
                                    <div class="single_product">
                                        <div class="product_thumb">
                                            <a th:href="@{'/product/'+${product.id}}">
                                                <img th:if="${not #strings.isEmpty(product.url_img)}"
                                                    th:src="${product.url_img}">
                                                <img th:if="${#strings.isEmpty(product.url_img)}"
                                                    src="/img/product/0/istockphoto-1409329028-170667a.jpg">
                                            </a>
                                            <div class="hot_img">
                                                <img src="/img/cart/span-hot.png">
                                            </div>
                                            <div class="product_action">
                                                <a href="#"> <i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
                                            </div>
                                        </div>
                                        <div class="product_content">
                                            <span class="product_price" th:text="${product.price+' VNĐ'}"></span>
                                            <h3 class="product_title"><a th:href="@{'/product/'+${product.id}}"
                                                    th:text="${product.name}"></a></h3>
                                        </div>
                                        <div class="product_info">
                                            <ul>
                                                <li><a href="#" title="Thêm vào danh sách yêu thích">Yêu thích</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#modal_box"
                                                        title="Quick view">Xem Chi Tiết</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--featured product area start-->

                    <!--blog area start-->
                    <div class="blog_area blog_two">
                        <div class="row">
                            <div class="col-12">
                                <div class="block_title">
                                    <h3>Bài viết</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <div class="single_blog">
                                    <div class="blog_thumb">
                                        <a href="blog-details.html"><img src="/img/blog/blog3.jpg" alt=""></a>
                                    </div>
                                    <div class="blog_content">
                                        <div class="blog_post">
                                            <ul>
                                                <li>
                                                    <a href="#">Tech</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <h3><a href="blog-details.html">When an unknown took a galley of type.</a></h3>
                                        <p>Distinctively simplify dynamic resources whereas prospective core
                                            competencies. Objectively pursue multidisciplinary human capital for
                                            interoperable.</p>
                                        <div class="post_footer">
                                            <div class="post_meta">
                                                <ul>
                                                    <li>Jun 20, 2018</li>
                                                    <li>3 Comments</li>
                                                </ul>
                                            </div>
                                            <div class="Read_more">
                                                <a href="blog-details.html">Read more <i
                                                        class="fa fa-angle-double-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--blog area end-->

                    <!--brand logo strat-->
                    <div class="brand_logo brand_two">
                        <div class="block_title">
                            <h3>Brands</h3>
                        </div>
                        <div class="row">
                            <div class="brand_active owl-carousel">
                                <div class="col-lg-2">
                                    <div class="single_brand">
                                        <a href="#"><img src="/img/brand/brand1.jpg" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single_brand">
                                        <a href="#"><img src="/img/brand/brand2.jpg" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single_brand">
                                        <a href="#"><img src="/img/brand/brand3.jpg" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single_brand">
                                        <a href="#"><img src="/img/brand/brand4.jpg" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single_brand">
                                        <a href="#"><img src="/img/brand/brand5.jpg" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="single_brand">
                                        <a href="#"><img src="/img/brand/brand6.jpg" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--brand logo end-->
                </div>
                <!--pos home section end-->
            </div>
            <!--pos page inner end-->
        </div>
    </div>
    <!--pos page end-->

    <!--footer area start-->
    <div th:replace="~{fragments/footer}"></div>
    <!--footer area end-->

    <!-- modal area start -->
    <div class="modal fade" id="modal_box" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal_body">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                <div class="modal_tab">
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="tab1" role="tabpanel">
                                            <div class="modal_tab_img">
                                                <a href="#"><img src="/img/product/product13.jpg" alt=""></a>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab2" role="tabpanel">
                                            <div class="modal_tab_img">
                                                <a href="#"><img src="/img/product/product14.jpg" alt=""></a>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab3" role="tabpanel">
                                            <div class="modal_tab_img">
                                                <a href="#"><img src="/img/product/product15.jpg" alt=""></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal_tab_button">
                                        <ul class="nav product_navactive" role="tablist">
                                            <li>
                                                <a class="nav-link active" data-toggle="tab" href="#tab1" role="tab"
                                                    aria-controls="tab1" aria-selected="false"><img
                                                        src="/img/cart/cart17.jpg" alt=""></a>
                                            </li>
                                            <li>
                                                <a class="nav-link" data-toggle="tab" href="#tab2" role="tab"
                                                    aria-controls="tab2" aria-selected="false"><img
                                                        src="/img/cart/cart18.jpg" alt=""></a>
                                            </li>
                                            <li>
                                                <a class="nav-link button_three" data-toggle="tab" href="#tab3"
                                                    role="tab" aria-controls="tab3" aria-selected="false"><img
                                                        src="/img/cart/cart19.jpg" alt=""></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-12">
                                <div class="modal_right">
                                    <div class="modal_title mb-10">
                                        <h2>Handbag feugiat</h2>
                                    </div>
                                    <div class="modal_price mb-10">
                                        <span class="new_price">$64.99</span>
                                        <span class="old_price">$78.99</span>
                                    </div>
                                    <div class="modal_content mb-10">
                                        <p>Short-sleeved blouse with feminine draped sleeve detail.</p>
                                    </div>
                                    <div class="modal_size mb-15">
                                        <h2>size</h2>
                                        <ul>
                                            <li><a href="#">s</a></li>
                                            <li><a href="#">m</a></li>
                                            <li><a href="#">l</a></li>
                                            <li><a href="#">xl</a></li>
                                            <li><a href="#">xxl</a></li>
                                        </ul>
                                    </div>
                                    <div class="modal_add_to_cart mb-15">
                                        <form action="#">
                                            <input min="0" max="100" step="2" value="1" type="number">
                                            <button type="submit">add to cart</button>
                                        </form>
                                    </div>
                                    <div class="modal_description mb-15">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        </p>
                                    </div>
                                    <div class="modal_social">
                                        <h2>Share this product</h2>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal area end -->




    <!-- all js here -->
    <script src="/js/vendor/jquery-1.12.0.min.js"></script>
    <script src="/js/popper.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/ajax-mail.js"></script>
    <script src="/js/plugins.js"></script>
    <script src="/js/main.js"></script>
</body>

</html>